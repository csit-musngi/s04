package com.zuitt;

import java.io.IOException;
import java.io.PrintWriter;

import jakarta.servlet.ServletContext;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

public class DetailsServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7758676644158652494L;
public void init() throws ServletException {
		
		System.out.println("******************************************");
		System.out.println(" DetailsServlet has been initialized. ");
		System.out.println("******************************************");
	}
public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException{
	String fname = System.getProperty("firstN");
	
	HttpSession session = req.getSession();
	String lname = (String) session.getAttribute("lastN");
	
	
	
	ServletContext srvContext = getServletContext();
	String email = (String) srvContext.getAttribute("emailAdd");
	
	String contact= req.getParameter("contact");
		
	PrintWriter output = res.getWriter();
	output.println(
			"<h1> Welcome to Phonebook</h1>"+
			"<p> First Name: "+fname+ "<p>"+
			"<p> Last Name: "+lname+ "<p>"+
			"<p> Email Address: "+email+ "<p>"+
			"<p> Contact Number: "+contact+ "<p>"

			);
	

}

public void destroy(){
	System.out.println("******************************************");
	System.out.println(" DetailsServlet has been destroyed. ");
	System.out.println("******************************************");
}


}
